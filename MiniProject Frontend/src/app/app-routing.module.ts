import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { StudentloginComponent } from './studentlogin/studentlogin.component';
import { HomeComponent } from './home/home.component';
import { SignupComponent } from './signup/signup.component';
import { AppComponent } from './app.component';
import { CompanyregistratationComponent } from './companyregistratation/companyregistratation.component';
import { GetAllCompaniesComponent } from './get-all-companies/get-all-companies.component';
import { MyprofileComponent } from './myprofile/myprofile.component';
import { AdmincompaniesComponent } from './admincompanies/admincompanies.component';
import { EditcompanydetailsComponent } from './editcompanydetails/editcompanydetails.component';

const routes: Routes = [
  {path:"",component:CompanyregistratationComponent},


  {path:"studentLogin",component:StudentloginComponent},
  {path:"homepage",component:HomeComponent},
  {path:"singup",component:SignupComponent},
{path:"app1",component:CompanyregistratationComponent},
{path:"app",component:AppComponent},
{path:"myprofile",component:MyprofileComponent},
{path:"admincompanies",component:AdmincompaniesComponent},
{path:"updatecompanies",component:EditcompanydetailsComponent},




{path:"getall",component:GetAllCompaniesComponent}



];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
