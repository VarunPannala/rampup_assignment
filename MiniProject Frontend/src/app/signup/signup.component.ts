import { Component, OnInit } from '@angular/core';
import { Student } from '../student';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { StudentserviceService } from '../studentservice.service';
import { Router } from '@angular/router';
import {LocalStorageService, SessionStorageService} from 'ngx-webstorage';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  title = 'miniproject5';
  admin:Student = new Student();
  form:FormGroup;
  submitted:boolean ;

 
  constructor(private snackBar:MatSnackBar,private fb:FormBuilder,private studentservice:StudentserviceService,private route:Router){

  }

  ngOnInit(): void {
    this.form=this.fb.group({

      mailId:['',Validators.compose([Validators.required,Validators.pattern('[a-zA-Z0-9.-_]{1,}@[a-zA-Z.-]{2,}[.]{1}[a-zA-Z]{2,}')])],
      password:['',Validators.compose([Validators.required,Validators.pattern('(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&].{8,}')])],
     
    });

  }




  
 

submitStudent(){


  this.studentservice.studentLogin(this.form.value.mailId,this.form.value.password)
  .subscribe(
    admin => {
      if(admin != null){
        //this.submitted = true;
        this.route.navigate(['homepage']);
        //alert("fdffdfg");
      }
      else{
        //this.submitted = false;
        alert("unsu")
      }
    }
  );



  
      
}

  
}
