import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { company } from '../company';
import { MatSnackBar } from '@angular/material/snack-bar';
import { CompanyserviceService } from '../companyservice.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-editcompanydetails',
  templateUrl: './editcompanydetails.component.html',
  styleUrls: ['./editcompanydetails.component.css']
})
export class EditcompanydetailsComponent implements OnInit {

  @Input() company:company;
  form1:FormGroup;
  com:company = new company();
  submitted:boolean = false;
  roles: string[] = ['Junior Analyst', 'Junior Analyst', 'Developer', 'Graduate Engineer Trainee', 'Cloud Engineer'];

 
  constructor(private snackBar:MatSnackBar,private fb:FormBuilder,private companyservice:CompanyserviceService,private route:Router){

  }

  ngOnInit(): void {  
    

    this.company=JSON.parse(localStorage.getItem('company'));
    
    console.log(this.company);
    
    
    


    this.form1=this.fb.group({

      companyId:[''],
      compnayName:[''],
      role:[''],
      annualPackage:[''],
      graduationPercentage:[''],
      secondaryEduPercentage:[''],
      primaryEduPercentage:[''],
      noofArrears:[''],


    });

    this.form1.setValue({

      companyId:this.company.companyId,
      compnayName:this.company.compnayName,
      role:this.company.role,
      annualPackage:this.company.annualPackage,
      graduationPercentage:this.company.minGraduationPercentage,
      secondaryEduPercentage:this.company.minSecondaryEduPercentage,
      primaryEduPercentage:this.company.minPrimaryEduPercentage,
      noofArrears:this.company.noofArrears,
    })

    this.form1.valueChanges.subscribe(console.log);
  }
  
 get(){
  this.company=JSON.parse(localStorage.getItem('company'));

  this.form1.setValue({

    companyId:this.company.companyId,
    compnayName:this.company.compnayName,
    role:this.company.role,
    annualPackage:this.company.annualPackage,
    graduationPercentage:this.company.minGraduationPercentage,
    secondaryEduPercentage:this.company.minSecondaryEduPercentage,
    primaryEduPercentage:this.company.minPrimaryEduPercentage,
    noofArrears:this.company.noofArrears,
  })

 }

registerStudent(){



  
  this.com=this.form1.value;
  this.companyservice.updateCompany(this.com).subscribe();
  this.route.navigate(['homepage']);


}

  
}

