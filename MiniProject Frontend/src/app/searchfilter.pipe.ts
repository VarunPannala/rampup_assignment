import { Pipe, PipeTransform } from '@angular/core';
import { company } from './company';

@Pipe({
  name: 'searchfilter'
})
export class SearchfilterPipe implements PipeTransform {
  //Company1:company[];
  transform(Company:company[],companyName:string): company[] {

    if(!Company||!companyName){
      return Company;
    }
    return null;

      return Company.filter(c=>c.compnayName.toLocaleLowerCase().includes(companyName.toLocaleLowerCase()));

  }

 // return Company.fil

}
