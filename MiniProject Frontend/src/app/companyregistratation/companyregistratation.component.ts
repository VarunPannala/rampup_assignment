import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Student } from '../student';
import { MatSnackBar } from '@angular/material/snack-bar';
import { StudentserviceService } from '../studentservice.service';
import { Router } from '@angular/router';
import { company } from '../company';
import { CompanyserviceService } from '../companyservice.service';

@Component({
  selector: 'app-companyregistratation',
  templateUrl: './companyregistratation.component.html',
  styleUrls: ['./companyregistratation.component.css']
})
export class CompanyregistratationComponent implements OnInit {

  
  form1:FormGroup;
  com:company = new company();
  submitted:boolean = false;
  roles: string[] = ['Senior Analyst', 'Junior Analyst', 'Developer', 'Graduate Engineer Trainee', 'Cloud Engineer'];

 
  constructor(private snackBar:MatSnackBar,private fb:FormBuilder,private companyservice:CompanyserviceService,private route:Router){

  }

  ngOnInit(): void {  
    this.form1=this.fb.group({

     
      compnayName:['',Validators.required],
      role:['',Validators.required],
      annualPackage:['',Validators.required],
      minGraduationPercentage:['',Validators.required],
      minSecondaryEduPercentage:['',Validators.required],
      minPrimaryEduPercentage:['',Validators.required],
      noofArrears:['',Validators.required],


    });

    //this.form1.valueChanges.subscribe(console.log);
  }
  
  

registerStudent(){
  this.com=this.form1.value;
  console.log(this.com);
  this.companyservice.companyRegister(this.com).subscribe();
  this.route.navigate(['homepage']); 


}

  
}

