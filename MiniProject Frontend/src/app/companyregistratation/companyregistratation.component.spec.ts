import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CompanyregistratationComponent } from './companyregistratation.component';

describe('CompanyregistratationComponent', () => {
  let component: CompanyregistratationComponent;
  let fixture: ComponentFixture<CompanyregistratationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CompanyregistratationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CompanyregistratationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
