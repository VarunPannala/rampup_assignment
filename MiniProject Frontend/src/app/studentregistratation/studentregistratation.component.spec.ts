import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StudentregistratationComponent } from './studentregistratation.component';

describe('StudentregistratationComponent', () => {
  let component: StudentregistratationComponent;
  let fixture: ComponentFixture<StudentregistratationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StudentregistratationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StudentregistratationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
