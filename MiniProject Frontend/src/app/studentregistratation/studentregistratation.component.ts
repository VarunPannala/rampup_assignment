import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';

import {FormGroup,FormControl,Validators, FormBuilder} from '@angular/forms';
import { StudentserviceService } from '../studentservice.service';
import { Student } from '../student';
import { Router } from '@angular/router';


@Component({
  selector: 'app-studentregistratation',
  templateUrl: './studentregistratation.component.html',
  styleUrls: ['./studentregistratation.component.css']
})
export class StudentregistratationComponent implements OnInit {

  form1:FormGroup;
  stu:Student = new Student();
  submitted:boolean = false;
  departments: string[] = ['CSE', 'EEE', 'MECH', 'ECE', 'IT'];

 
  constructor(private snackBar:MatSnackBar,private fb:FormBuilder,private studentservice:StudentserviceService,private route:Router){

  }

  ngOnInit(): void {  
    this.form1=this.fb.group({

      mailId:['',Validators.compose([Validators.required,Validators.pattern('[a-zA-Z0-9.-_]{1,}@[a-zA-Z.-]{2,}[.]{1}[a-zA-Z]{2,}')])],
      password:['',Validators.compose([Validators.required,Validators.pattern('(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&].{8,}')])],
      studentName:['',Validators.required,Validators.minLength(3)],
      department:['',Validators.required],
      graduationPercentage:['',Validators.required],
      secondaryEduPercentage:['',Validators.required],
      primaryEduPercentage:['',Validators.required],
      noofArrears:['',Validators.required],


    });

    this.form1.valueChanges.subscribe(console.log);
  }
  
 

registerStudent(){
  this.stu=this.form1.value;
  this.studentservice.studentRegister(this.stu)
  .subscribe();
  this.route.navigate(['homepage']);


}

  
}

