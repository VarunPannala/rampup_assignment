import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Student } from './student';
import { company } from './company';

@Injectable({
  providedIn: 'root'
})
export class StudentserviceService {

  private baseUrl = 'http://localhost:8080/api/studentauth';
  private baseUrl1 = 'http://localhost:8080/api/student';

  private baseUrl2 = 'http://localhost:8080/api/getallcompanies';
  private baseurl3='http://localhost:8080/api/getallUnappliedcompanies'

  constructor(private http:HttpClient) { }
  private dUrl = 'http://localhost:8080/api/admins';


  studentLogin(mailId:String,password:String):Observable<any>{
    return this.http.get(`${this.baseUrl}/${mailId}/${password}`);
  }

  studentRegister(student:any):Observable<any>{

    return this.http.post(this.baseUrl1,student);

    

  }

  studentAppliedCompanies(studentId:Number):Observable<company[]>{
    return this.http.get<company[]>(`${this.baseUrl2}/${studentId}`);
  }

  studentunappliedCompanies(studentId:Number):Observable<company[]>{

    return this.http.get<company[]>(`${this.baseurl3}/${studentId}`);


  }

}
