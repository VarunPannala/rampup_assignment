import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { company } from './company';
import { Student } from './student';

@Injectable({
  providedIn: 'root'
})
export class CompanyserviceService {
  private baseUrl = 'http://localhost:8080/api/showcompanies';
  private base = 'http://localhost:8080/api/applycompany';
  private baseU = 'http://localhost:8080/api/company';
  private baseU1 = 'http://localhost:8080/api/delete';
  
  private baseU3 =  'http://localhost:8080/api/updatecompany';


  constructor(private http:HttpClient) { }


  //getAllCompanies():Observable<company[]>{

    //return this.http.get(this.baseUrl);


    getHangerList():Observable<company[]> {
     return this.http.get<company[]>(this.baseUrl);
    }

    applycompany1(student:Student):Observable<any> {
      return this.http.post(this.base,student);
     }
     companyRegister(company:company):Observable<any>{

      console.log(company);
      return this.http.post(this.baseU,company);
  

     }


     deleteCompany(companyId:Number):any{
      return this.http.delete(`${this.baseU1}/${companyId}`);

       
     }

     updateCompany(company:any):any{
      return this.http.post(this.baseU3,company);

       
     }
  

  
}
