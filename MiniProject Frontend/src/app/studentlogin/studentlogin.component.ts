import { Component, OnInit, Output } from '@angular/core';
import { Student } from '../student';

import { MatSnackBar } from '@angular/material/snack-bar';
import {FormGroup,FormControl, Validators, FormBuilder} from '@angular/forms';
import { StudentserviceService } from '../studentservice.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-studentlogin',
  templateUrl: './studentlogin.component.html',
  styleUrls: ['./studentlogin.component.css']
})
export class StudentloginComponent implements OnInit {
  title = 'miniproject5';
  admin:Student = new Student();
  form:FormGroup;
  loggedIn:boolean;
  student:Student;
  hide = true;

 
  constructor(private snackBar:MatSnackBar,private fb:FormBuilder,private studentservice:StudentserviceService,private route:Router){

  }

  ngOnInit(): void {
    this.form=this.fb.group({

      mailId:['',Validators.compose([Validators.required,Validators.pattern('[a-zA-Z0-9.-_]{1,}@[a-zA-Z.-]{2,}[.]{1}[a-zA-Z]{2,}')])],
      password:['',Validators.compose([Validators.required,Validators.pattern('(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&].{8,}')])],

    });



  }



 

submitStudent(){


  this.studentservice.studentLogin(this.form.value.mailId,this.form.value.password)
  .subscribe(
    admin => {
      if(admin != null){
        //this.loggedIn = true;
        localStorage.setItem('loggedIn', 'true');
        localStorage.setItem('log', 'true');
        
        localStorage.setItem('student',JSON.stringify(admin));
        this.student=JSON.parse(localStorage.getItem('student'));

        
if(this.student.mailId=="admin@gmail.com"){
  localStorage.setItem('admin', 'true');

}

        // console.log(this.student);
        
        //this.route.navigate(['app']);
        //window.location.reload();
        //alert("Succesfully loggedIn")

        this.route.navigate(['homepage']);




       
      }
      else{
        //this.submitted = false;
        alert("Login credentials are incorrect")
      }
    }
  );



  
      
}
get passwordInput() { return this.form.get('password'); }  


  
}