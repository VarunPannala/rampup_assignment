import { Component, OnInit } from '@angular/core';
import { Student } from '../student';
import { StudentserviceService } from '../studentservice.service';
import { company } from '../company';

@Component({
  selector: 'app-myprofile',
  templateUrl: './myprofile.component.html',
  styleUrls: ['./myprofile.component.css']
})
export class MyprofileComponent implements OnInit {

  student:Student;
  com:company[];

  constructor(private st:StudentserviceService) { }

  ngOnInit(): void {

    this.student=JSON.parse(localStorage.getItem('student'));
   this.st.studentAppliedCompanies(this.student.studentId).subscribe(
    company1=>{ this.com=company1}

   );
   console.log(this.com);

  }

}
