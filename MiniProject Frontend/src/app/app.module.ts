import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatCardModule} from '@angular/material/card';
import {MatTabsModule} from '@angular/material/tabs';
import {MatInputModule} from '@angular/material/input';
import {MatButtonModule} from '@angular/material/button';
import {MatIconModule} from '@angular/material/icon';
import {FormsModule,ReactiveFormsModule} from '@angular/forms';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {MatListModule} from '@angular/material/list';
import {MatSelectModule} from '@angular/material/select';
import { HttpClientModule } from '@angular/common/http';

import { StudentregistratationComponent } from './studentregistratation/studentregistratation.component';
import { CompanyregistratationComponent } from './companyregistratation/companyregistratation.component';
import { StudentloginComponent } from './studentlogin/studentlogin.component';
import { SignupComponent } from './signup/signup.component';
import {MatToolbarModule} from '@angular/material/toolbar';
import { RouterModule } from '@angular/router';
import { GetAllCompaniesComponent } from './get-all-companies/get-all-companies.component';
import {MatTableModule} from '@angular/material/table';
import {MatPaginatorModule} from '@angular/material/paginator';
import {​​​​​ JwPaginationModule }​​​​​ from 'jw-angular-pagination';
import { MyprofileComponent } from './myprofile/myprofile.component';
import{Searchfilter1Pipe} from './searchfilter1.pipe';
import {MatDialogModule} from '@angular/material/dialog';
import { AdmincompaniesComponent } from './admincompanies/admincompanies.component';
import { EditcompanydetailsComponent } from './editcompanydetails/editcompanydetails.component';

@NgModule({
  declarations: [
    AppComponent, 
    StudentregistratationComponent, CompanyregistratationComponent, StudentloginComponent, SignupComponent, GetAllCompaniesComponent, MyprofileComponent,Searchfilter1Pipe, AdmincompaniesComponent, EditcompanydetailsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatCardModule,
    MatTabsModule,
    MatInputModule,
    MatButtonModule,
    MatSelectModule,
    MatListModule,
    ReactiveFormsModule,
    MatIconModule,
    FormsModule,
    HttpClientModule,
    MatToolbarModule,
    MatPaginatorModule,
    MatDialogModule,
    JwPaginationModule,
    MatTableModule,
    MatSnackBarModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {Searchfilter1Pipe }
