import { company } from './company';

export class Student{
    studentId:Number;
    mailId:String;
    password:String;
    studentName:String;
    department:String;
    graduationPercentage:Number;
    secondaryEduPercentage:Number;
    primaryEduPercentage:Number;
    noofArrears:Number;
    companies:company[];
}