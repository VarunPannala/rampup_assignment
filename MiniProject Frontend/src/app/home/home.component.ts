import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  login=localStorage.getItem('loggedIn');

  l:boolean=(this.login =="true");

  
  constructor(private route:Router) { 

    

  }
  
  ngOnInit(): void {

    if((localStorage.getItem('log') =="true") ){
      window.location.reload();

      localStorage.setItem('log', 'false');
      //localStorage.setItem('admin', 'false');

        //window.location.reload();

 }
    
  }

  
logout(){


  localStorage.clear();
  this.l=false;

  this.route.navigate(['homepage']);


      
}


}
