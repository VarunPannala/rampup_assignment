import { Component, Input } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Student } from './student';
import {FormGroup,FormControl, Validators, FormBuilder} from '@angular/forms';
import { StudentserviceService } from './studentservice.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl:'./app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'miniproject5';
  admin:Student = new Student();
  form:FormGroup;
  submitted:boolean ;
 login:boolean=(localStorage.getItem('loggedIn') =="true");
 admin1:boolean=(localStorage.getItem('admin') =="true");
login1:boolean=true;

l:boolean=(!this.login &&  this.admin1);
l1:boolean=(this.login &&  !this.admin1);
l3:boolean=(this.login ||  this.admin1);


  constructor(private snackBar:MatSnackBar,private fb:FormBuilder,private studentservice:StudentserviceService,private route:Router){

  }

  ngOnInit(): void {

  }



  


  

logout(){


  localStorage.clear();
  localStorage.setItem('loggedIn', 'false');
  //localStorage.setItem('log', 'true');
  localStorage.setItem('admin', 'false');
  this.l1=false;
this.admin1=false;
  this.login=false;
  this.route.navigate(['homepage']);


      
}

  
} 

