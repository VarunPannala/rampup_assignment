import { Pipe, PipeTransform } from '@angular/core';
import { company } from './company';
import { SearchfilterPipe } from './searchfilter.pipe';

@Pipe({
  name: 'searchfilter1' 
})
export class Searchfilter1Pipe implements PipeTransform {
  //c:d;
  transform(Company:company[],compnayName:string): company[] {

    if(!Company||!compnayName){
      return Company;
    }
    return null;

      return Company.filter(c=>c.compnayName.toLocaleLowerCase().includes(compnayName.toLocaleLowerCase()));

  }

}
