package com.hcl.springbootAngular.controller;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.springbootAngular.dao.CompanyRepository;
import com.hcl.springbootAngular.dao.StudentRepository;
import com.hcl.springbootAngular.model.Company;
import com.hcl.springbootAngular.model.PlacementOfficer;
import com.hcl.springbootAngular.model.Student;
import com.hcl.springbootAngular.service.CompanyService;
import com.hcl.springbootAngular.service.OfficerService;
import com.hcl.springbootAngular.service.StudentService;

@CrossOrigin(origins="http://localhost:4200")
@RequestMapping("/api")
@RestController
public class MainController {

	@Autowired
	StudentService studentService;
	@PostMapping(value = "/student")
	public ResponseEntity<Student> postStudent(@RequestBody Student student) {
		try {
			
			return new ResponseEntity<>(studentService.studentRegistration(student), HttpStatus.CREATED);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.EXPECTATION_FAILED);
		}
	}
	
	@Autowired
	CompanyService companyService;
	
	@PostMapping(value = "/company")
	public ResponseEntity<Company> postCompany(@RequestBody Company company) {
		try {
			
			return new ResponseEntity<>(companyService.companyRegistration(company), HttpStatus.CREATED);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.EXPECTATION_FAILED);
		}
	}  
	
	@Autowired    
	StudentService studentService1;
	@GetMapping(value = "/studentauth/{mailId}/{password}")
	public ResponseEntity<Student> postStudentAuth(@PathVariable("mailId") String mailId,@PathVariable("password") String password) {
		try {
			
			Student student=studentService1.studentAuthentication(mailId,password);
			 System.out.println(student);	

			if(student!=null && student.getPassword().equals(password)) {
			 System.out.println(student);	
			 return new ResponseEntity<>(student,HttpStatus.CREATED);
			}
			else
				return new ResponseEntity<>(null, HttpStatus.NO_CONTENT); 
			
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.EXPECTATION_FAILED);
		}
	}
	
	
	@Autowired
	OfficerService officerService;
	@GetMapping(value = "/officeauth/{mailId}/{password}")
	public ResponseEntity<PlacementOfficer> postOfficeAuth(@PathVariable("mailId") String mailId,@PathVariable("password") String password) {
		try {
			
			PlacementOfficer placementOfficer=officerService.placementOfficerAuthentication(mailId,password);


			if(placementOfficer.getPassword().equals(password)) {
			 System.out.println(placementOfficer);
			return new ResponseEntity<>(officerService.placementOfficerAuthentication(mailId, password), HttpStatus.CREATED);
			}
			
			else
				return new ResponseEntity<>(null, HttpStatus.NO_CONTENT); 
			
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.EXPECTATION_FAILED);
		}
	}
	
	@DeleteMapping("/delete/{companyId}")
	public ResponseEntity<Integer> deleteCompany(@PathVariable("companyId") int companyId) {
		try {
			return new ResponseEntity<>(companyService.deleteCompany(companyId), HttpStatus.CREATED);

	 	} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.EXPECTATION_FAILED);
		}
	}
	
	
	
	
	@PostMapping(value = "/updatecompany")
	public ResponseEntity<Company> updateCompany(@RequestBody Company company) {
		try {
			
			return new ResponseEntity<>(companyService.updateCompanyDetails(company), HttpStatus.CREATED);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.EXPECTATION_FAILED);
		}
	}
	
	@Autowired
	CompanyRepository companyRepository;
	@Autowired
	StudentRepository  	studentRepository;

	

	
	@GetMapping(value = "/showcompanies")
	public ResponseEntity<Iterable<Company>> showCompany( ) {
		try {
			
			return new ResponseEntity<>(companyRepository.findAll(), HttpStatus.CREATED);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.EXPECTATION_FAILED);
		}
	}
	
	@GetMapping(value = "/showstudents")
	public ResponseEntity<Page<Student>> showStudents(Pageable pagable) {
		try {
			
			return new ResponseEntity<>(studentRepository.findAll(pagable), HttpStatus.CREATED);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.EXPECTATION_FAILED);
		}
	}
	
	
	
	
	@GetMapping(value = "/searchcompanie")
	public ResponseEntity<Iterable<Company>> searchCompany(String compnayName ) {
		try {
			
			return new ResponseEntity<>(companyRepository.findByCompnayName(compnayName), HttpStatus.CREATED);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.EXPECTATION_FAILED);
		}
		
	}
	
	
	
	
	
	
	@GetMapping(value ="/getallUnappliedcompanies/{studentId}")

	public ResponseEntity<List<Company>> getAllUnCompanies(@PathVariable("studentId") int studentId ) {
		try {
			  List<Company> c;
			  List<Company> c1;
			  List<Company> r;


			  Student student=null;
               System.out.println(studentRepository.findById(studentId));
               Optional<Student> students=studentRepository.findById(studentId);
              // System.out.println(student.map(s->s.getCompanies()));   
               if(students.isPresent())
            	   student=students.get();
               
               
               
               c1=(List<Company>) companyRepository.findAll();
               c=student.getCompanies();
               
	          c1.removeAll(c);
			return new ResponseEntity<> (c1,HttpStatus.CREATED);
			
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.EXPECTATION_FAILED);
		}
	}

	  
	
	
	
	@PostMapping(value = "/editstudent")
	public ResponseEntity<Student> updateStudent(@RequestBody Student student) {
		try {
			
			return new ResponseEntity<>(studentService.updateStudentDetails(student), HttpStatus.CREATED);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.EXPECTATION_FAILED);
		}  
	}  
	
	@PostMapping(value = "/applycompany")
	public ResponseEntity<Student> applyCompany(@RequestBody Student student) {
		try {
			int id=student.getStudentId();
			System.out.println(id);
			Optional<Student> student1=studentRepository.findById(id);
			Student student2=null;
			 if(student1.isPresent())
          	   student2=student1.get();
			   
			List<Company> companies=student.getCompanies();
			
				student2.getCompanies().addAll(companies);
				
			
			    
            
			return new ResponseEntity<>(studentService.applyCompany(student2), HttpStatus.CREATED);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.EXPECTATION_FAILED);
		}
	}
	
	

	@GetMapping(value ="/getallcompanies/{studentId}")

	public ResponseEntity<List<Company>> getAllCompanies(@PathVariable("studentId") int studentId ) {
		try {
			  List<Company> c;
			  Student student=null;
               System.out.println(studentRepository.findById(studentId));
               Optional<Student> students=studentRepository.findById(studentId);
              // System.out.println(student.map(s->s.getCompanies()));   
               if(students.isPresent())
            	   student=students.get();
               

               System.out.println(student.getCompanies());
	
			return new ResponseEntity<>(student.getCompanies(),HttpStatus.CREATED);
			
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.EXPECTATION_FAILED);
		}
	}
}