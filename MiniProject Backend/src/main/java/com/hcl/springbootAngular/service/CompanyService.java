package com.hcl.springbootAngular.service;

import java.util.List;

import org.springframework.boot.autoconfigure.data.web.SpringDataWebProperties.Pageable;

import com.hcl.springbootAngular.model.Company;
import com.hcl.springbootAngular.model.PlacementOfficer;
import com.hcl.springbootAngular.model.Student;

public interface CompanyService {
	public Company companyRegistration(Company company);
	public int     deleteCompany(int companyId);
	public Company updateCompanyDetails(Company company);
	public List<Company> findAll();
	public List<Company> searchCompany(String compnayName);


}
