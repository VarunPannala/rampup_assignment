package com.hcl.springbootAngular.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hcl.springbootAngular.dao.OfficerRepository;
import com.hcl.springbootAngular.model.PlacementOfficer;

@Service 
public class OfficerServiceImpl implements OfficerService {

	@Autowired
	OfficerRepository officerRepository ;

	
	@Override
	public PlacementOfficer placementOfficerAuthentication(String mailId, String password) {
		PlacementOfficer placementOfficer=officerRepository.findByMailId(mailId);
		return placementOfficer;
	}
	
	

}
