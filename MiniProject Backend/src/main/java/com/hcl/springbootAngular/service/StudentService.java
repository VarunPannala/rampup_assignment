package com.hcl.springbootAngular.service;

import com.hcl.springbootAngular.model.Company;
import com.hcl.springbootAngular.model.Student;

public interface StudentService {
	public Student studentRegistration(Student student);
	public Student studentAuthentication(String mailId,String password);
	public Student updateStudentDetails(Student student);
	public Student applyCompany(Student student);

	
	

}
