package com.hcl.springbootAngular.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hcl.springbootAngular.dao.StudentRepository;
import com.hcl.springbootAngular.model.Student;

@Service
public class StudentServiceImpl implements StudentService {

	@Autowired
	StudentRepository StudentRepository1; 
	
	@Override   
	public Student studentRegistration(Student student) {
		   
			return StudentRepository1
			.save(new Student(student.getMailId(),student.getPassword(),student.getStudentName(),student.getDepartment(),student.getGraduationPercentage(),student.getSecondaryEduPercentage(),student.getPrimaryEduPercentage(),student.getNoofArrears()));
		}

	@Override
	public Student studentAuthentication(String mailId, String password) {
		Student student=StudentRepository1.findByMailId(mailId);
		return student;

	}

	@Override
	public Student updateStudentDetails(Student student) {
		return StudentRepository1
				.save(new Student(student.getStudentId(),student.getMailId(),student.getPassword(),student.getStudentName(),student.getDepartment(),student.getGraduationPercentage(),student.getSecondaryEduPercentage(),student.getPrimaryEduPercentage(),student.getNoofArrears()));
			}

	@Override
	public Student applyCompany(Student student) {
		return StudentRepository1
				.save(new Student(student.getStudentId(),student.getMailId(),student.getPassword(),student.getStudentName(),student.getDepartment(),student.getGraduationPercentage(),student.getSecondaryEduPercentage(),student.getPrimaryEduPercentage(),student.getNoofArrears(),student.getCompanies()));
			}
	
	
	
	 

	}
	
	

