package com.hcl.springbootAngular.service;

import com.hcl.springbootAngular.model.PlacementOfficer;

public interface OfficerService {
	
	public PlacementOfficer placementOfficerAuthentication(String mailId,String password);


}
