package com.hcl.springbootAngular.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.data.web.SpringDataWebProperties.Pageable;
import org.springframework.stereotype.Service;

import com.hcl.springbootAngular.dao.CompanyRepository;
import com.hcl.springbootAngular.dao.OfficerRepository;
import com.hcl.springbootAngular.model.Company;
import com.hcl.springbootAngular.model.PlacementOfficer;
import com.hcl.springbootAngular.model.Student;

@Service
public class CompanyServiceImp implements CompanyService {

	@Autowired
	CompanyRepository companyRepository;
	
	
	@Override
	public Company companyRegistration(Company company) {
		return companyRepository
				.save(new Company(company.getCompnayName(),company.getRole(),company.getAnnualPackage(),company.getMinGraduationPercentage(),company.getMinSecondaryEduPercentage(),company.getMinPrimaryEduPercentage(),company.getNoofArrears()));
	}


	@Override
	public int deleteCompany(int companyId) {
		companyRepository.deleteById(companyId);
		return companyId;
		
	}


	@Override
	public Company updateCompanyDetails(Company company) {
		return companyRepository
				.save(new Company(company.getCompanyId(),company.getCompnayName(),company.getRole(),company.getAnnualPackage(),company.getMinGraduationPercentage(),company.getMinSecondaryEduPercentage(),company.getMinPrimaryEduPercentage(),company.getNoofArrears()));
	}


	@Override
	public List<Company> findAll() {
		// TODO Auto-generated method stub
		return (List<Company>) companyRepository.findAll();
	}


	@Override
	public List<Company> searchCompany(String compnayName) {
		
		return (List<Company>) companyRepository.findByCompnayName(compnayName);
		//return c;
	}


	
		
	
	
	

	
	
	

}
