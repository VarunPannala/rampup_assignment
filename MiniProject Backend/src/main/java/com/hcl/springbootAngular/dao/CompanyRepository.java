package com.hcl.springbootAngular.dao;
import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.hcl.springbootAngular.model.Company;
import com.hcl.springbootAngular.model.PlacementOfficer;
import com.hcl.springbootAngular.model.Student;
@Repository
public interface CompanyRepository extends PagingAndSortingRepository<Company,Integer> {
  
	public List<Company> findByCompnayName(String compnayName);
	

}
   