package com.hcl.springbootAngular.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.hcl.springbootAngular.model.Company;
import com.hcl.springbootAngular.model.PlacementOfficer;
import com.hcl.springbootAngular.model.Student;

public interface OfficerRepository  extends PagingAndSortingRepository<PlacementOfficer,Integer> {

	
	public PlacementOfficer findByMailId(String mailId);

}
