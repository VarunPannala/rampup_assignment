package com.hcl.springbootAngular.dao;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.hcl.springbootAngular.model.Student;

public interface StudentRepository  extends PagingAndSortingRepository<Student,Integer>{
	
	public Student findByMailId(String mailId);

}
