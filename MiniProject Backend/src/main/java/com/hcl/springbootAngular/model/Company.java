package com.hcl.springbootAngular.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "Company")
public class Company {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int companyId;
	private String compnayName;
	private String role;
	private double annualPackage;
	private double minGraduationPercentage;
	private double minSecondaryEduPercentage;
	private double minPrimaryEduPercentage;
	private int noofArrears;
	private String status;
    @JsonIgnore

	@ManyToMany(mappedBy="Companies")
	private List<Student> students;

	public Company(String compnayName, String role, double annualPackage, double minGraduationPercentage,
			double minSecondaryEduPercentage, double minPrimaryEduPercentage, int noofArrears) {
		super();
		this.compnayName = compnayName;
		this.role = role;
		this.annualPackage = annualPackage;
		this.minGraduationPercentage = minGraduationPercentage;
		this.minSecondaryEduPercentage = minSecondaryEduPercentage;
		this.minPrimaryEduPercentage = minPrimaryEduPercentage;
		this.noofArrears = noofArrears;
	}

	public Company() {
		super();
		// TODO Auto-generated constructor stub
	}

	public int getCompanyId() {
		return companyId;
	}

	public void setCompanyId(int companyId) {
		this.companyId = companyId;
	}

	public String getCompnayName() {
		return compnayName;
	}

	public void setCompnayName(String compnayName) {
		this.compnayName = compnayName;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public double getAnnualPackage() {
		return annualPackage;
	}

	public void setAnnualPackage(double annualPackage) {
		this.annualPackage = annualPackage;
	}

	public double getMinGraduationPercentage() {
		return minGraduationPercentage;
	}

	public void setMinGraduationPercentage(double minGraduationPercentage) {
		this.minGraduationPercentage = minGraduationPercentage;
	}

	public double getMinSecondaryEduPercentage() {
		return minSecondaryEduPercentage;
	}

	public void setMinSecondaryEduPercentage(double minSecondaryEduPercentage) {
		this.minSecondaryEduPercentage = minSecondaryEduPercentage;
	}

	public double getMinPrimaryEduPercentage() {
		return minPrimaryEduPercentage;
	}

	public void setMinPrimaryEduPercentage(double minPrimaryEduPercentage) {
		this.minPrimaryEduPercentage = minPrimaryEduPercentage;
	}

	public int getNoofArrears() {
		return noofArrears;
	}

	public void setNoofArrears(int noofArrears) {
		this.noofArrears = noofArrears;
	}

	public List<Student> getStudents() {
		return students;
	}

	public void setStudents(List<Student> students) {
		this.students = students;
	}

	public Company(int companyId, String compnayName, String role, double annualPackage, double minGraduationPercentage,
			double minSecondaryEduPercentage, double minPrimaryEduPercentage, int noofArrears) {
		super();
		this.companyId = companyId;
		this.compnayName = compnayName;
		this.role = role;
		this.annualPackage = annualPackage;
		this.minGraduationPercentage = minGraduationPercentage;
		this.minSecondaryEduPercentage = minSecondaryEduPercentage;
		this.minPrimaryEduPercentage = minPrimaryEduPercentage;
		this.noofArrears = noofArrears;
	}

	@Override
	public String toString() {
		return "Company [companyId=" + companyId + ", compnayName=" + compnayName + ", role=" + role
				+ ", annualPackage=" + annualPackage + ", minGraduationPercentage=" + minGraduationPercentage
				+ ", minSecondaryEduPercentage=" + minSecondaryEduPercentage + ", minPrimaryEduPercentage="
				+ minPrimaryEduPercentage + ", noofArrears=" + noofArrears + "]";
	}
	
	

}
