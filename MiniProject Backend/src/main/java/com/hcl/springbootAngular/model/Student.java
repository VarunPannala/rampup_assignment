package com.hcl.springbootAngular.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "Student")
public class Student {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int studentId;
	@Column(unique = true)
	private String mailId;
	private String password;
	private String studentName;
	private String department;
	private double graduationPercentage;
	private double secondaryEduPercentage;
	private double primaryEduPercentage;
	@Column(columnDefinition = "int default 0")
	private int noofArrears;

	@ManyToMany
	@JoinTable(name = "student_companies", inverseJoinColumns = @JoinColumn(name = "companyId"), joinColumns = @JoinColumn(name = "studentId"))
	private List<Company> Companies;

	// @ManyToMany
	// @JoinTable(name="student_companies",joinColumns=
	// {@JoinColumn(name="studentId")},inverseJoinColumns=
	// {@JoinColumn(name="companyId")})
	// private List<Company> Companies;

	public Student() {  
		super();
		// TODO Auto-generated constructor stub
	}

	public Student(int studentId, String mailId, String password, String studentName, String department,
			double graduationPercentage, double secondaryEduPercentage, double primaryEduPercentage, int noofArrears,
			List<Company> companies) {
		super();
		this.studentId = studentId;
		this.mailId = mailId;
		this.password = password;
		this.studentName = studentName;
		this.department = department;
		this.graduationPercentage = graduationPercentage;
		this.secondaryEduPercentage = secondaryEduPercentage;
		this.primaryEduPercentage = primaryEduPercentage;
		this.noofArrears = noofArrears;
		Companies = companies;
	}

	public Student(String mailId, String password, String studentName, String department, double graduationPercentage,
			double secondaryEduPercentage, double primaryEduPercentage, int noofArrears) {
		super();
		this.mailId = mailId;
		this.password = password;
		this.studentName = studentName;
		this.department = department;
		this.graduationPercentage = graduationPercentage;
		this.secondaryEduPercentage = secondaryEduPercentage;
		this.primaryEduPercentage = primaryEduPercentage;
		this.noofArrears = noofArrears;
	}

	public Student(int studentId, String mailId, String password, String studentName, String department,
			double graduationPercentage, double secondaryEduPercentage, double primaryEduPercentage, int noofArrears) {
		super();
		this.studentId = studentId;
		this.mailId = mailId;
		this.password = password;
		this.studentName = studentName;
		this.department = department;
		this.graduationPercentage = graduationPercentage;
		this.secondaryEduPercentage = secondaryEduPercentage;
		this.primaryEduPercentage = primaryEduPercentage;
		this.noofArrears = noofArrears;
	}

	public int getStudentId() {
		return studentId;
	}

	public void setStudentId(int studentId) {
		this.studentId = studentId;
	}

	public String getMailId() {
		return mailId;
	}

	public void setMailId(String mailId) {
		this.mailId = mailId;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getStudentName() {
		return studentName;
	}

	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public double getGraduationPercentage() {
		return graduationPercentage;
	}

	public void setGraduationPercentage(double graduationPercentage) {
		this.graduationPercentage = graduationPercentage;
	}

	public double getSecondaryEduPercentage() {
		return secondaryEduPercentage;
	}

	public void setSecondaryEduPercentage(double secondaryEduPercentage) {
		this.secondaryEduPercentage = secondaryEduPercentage;
	}

	public double getPrimaryEduPercentage() {
		return primaryEduPercentage;
	}

	public void setPrimaryEduPercentage(double primaryEduPercentage) {
		this.primaryEduPercentage = primaryEduPercentage;
	}

	public int getNoofArrears() {
		return noofArrears;
	}

	public void setNoofArrears(int noofArrears) {
		this.noofArrears = noofArrears;
	}

	public List<Company> getCompanies() {
		return Companies;
	}

	public void setCompanies(List<Company> companies) {
		Companies = companies;
	}

	@Override
	public String toString() {
		return "Student [studentId=" + studentId + ", mailId=" + mailId + ", password=" + password + ", studentName="
				+ studentName + ", department=" + department + ", graduationPercentage=" + graduationPercentage
				+ ", secondaryEduPercentage=" + secondaryEduPercentage + ", primaryEduPercentage="
				+ primaryEduPercentage + ", noofArrears=" + noofArrears + "]";
	}

}
