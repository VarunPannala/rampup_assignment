package com.hcl.springbootAngular.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity 
@Table(name="PlacementOfficer")
public class PlacementOfficer {
	 @Id
	   @GeneratedValue(strategy=GenerationType.AUTO)
	   private int officerId;
	   @Column(unique = true)
	   private String mailId;
	   private String password;
	public PlacementOfficer() {
		super();
		// TODO Auto-generated constructor stub
	}
	public String getMailId() {
		return mailId;
	}
	public void setMailId(String mailId) {
		this.mailId = mailId;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public PlacementOfficer(String mailId, String password) {
		super();
		this.mailId = mailId;
		this.password = password;
	}
	@Override
	public String toString() {
		return "PlacementOfficer [officerId=" + officerId + ", mailId=" + mailId + ", password=" + password + "]";
	}
	
	   

}
