package com.model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity 
@Table(name="UserBooking")
public class UserBooking {

	
	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private int Booking_id;
    @ManyToOne(cascade=CascadeType.ALL)
    @JoinColumn(name="userId")
    private User user;
    @ManyToOne(cascade=CascadeType.ALL)
    @JoinColumn(name="vehicleid")
    private Vehicle vehicle;
    @OneToOne(cascade=CascadeType.ALL)
    @JoinColumn(name="driverid")
    private Driver driver;
    private String Source;
    private String Destination;
	public UserBooking() {
		super();
		// TODO Auto-generated constructor stub
	}
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return super.toString();
	}
	public UserBooking(int booking_id, User user, Vehicle vehicle, Driver driver, String source, String destination) {
		super();
		Booking_id = booking_id;
		this.user = user;
		this.vehicle = vehicle;
		this.driver = driver;
		Source = source;
		Destination = destination;
	}
	public int getBooking_id() {
		return Booking_id;
	}
	public void setBooking_id(int booking_id) {
		Booking_id = booking_id;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public Vehicle getVehicle() {
		return vehicle;
	}
	public void setVehicle(Vehicle vehicle) {
		this.vehicle = vehicle;
	}
	public Driver getDriver() {
		return driver;
	}
	public void setDriver(Driver driver) {
		this.driver = driver;
	}
	public String getSource() {
		return Source;
	}
	public void setSource(String source) {
		Source = source;
	}
	public String getDestination() {
		return Destination;
	}
	public void setDestination(String destination) {
		Destination = destination;
	}
	
}
