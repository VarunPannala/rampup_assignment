package com.model;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity 
@Table(name="Vehicle")
public class Vehicle {

	   @Id
	   @GeneratedValue(strategy=GenerationType.AUTO)
	   private int vehicleId;
	   private String vehicleName;
	   private String vehicleType;
	   @GeneratedValue(strategy=GenerationType.AUTO)
	   private int vehicleRegNo;
	   
	   @ManyToMany(mappedBy="Vehicles")
	   private List<User> Users;
	   
	   
	public Vehicle(int vehicleId, String vehicleName, String vehicleType, int vehicleRegNo, List<User> users) {
		super();
		this.vehicleId = vehicleId;
		this.vehicleName = vehicleName;
		this.vehicleType = vehicleType;
		this.vehicleRegNo = vehicleRegNo;
		Users = users;
	}
	
	
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return vehicleId+" "+vehicleName+" "+vehicleType+" "+vehicleRegNo;
	}



	public Vehicle() {
		super();
		// TODO Auto-generated constructor stub
	}
	public int getVehicleId() {
		return vehicleId;
	}
	public void setVehicleId(int vehicleId) {
		this.vehicleId = vehicleId;
	}
	public String getVehicleName() {
		return vehicleName;
	}
	public void setVehicleName(String vehicleName) {
		this.vehicleName = vehicleName;
	}
	public String getVehicleType() {
		return vehicleType;
	}
	public void setVehicleType(String vehicleType) {
		this.vehicleType = vehicleType;
	}
	public int getVehicleRegNo() {
		return vehicleRegNo;
	}
	public void setVehicleRegNo(int vehicleRegNo) {
		this.vehicleRegNo = vehicleRegNo;
	}
	public List<User> getUsers() {
		return Users;
	}
	public void setUsers(List<User> users) {
		Users = users;
	}

}