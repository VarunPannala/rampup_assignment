package com.model;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity 
@Table(name="Driver")
public class Driver {
	
	   @Id
	   @GeneratedValue(strategy=GenerationType.AUTO)
	   private int driverId;
	   private String driverName;
	   private int driverLicenceNo;
	
	   
	   public Driver(int driverId, String driverName, int driverLicenceNo) {
		super();
		this.driverId = driverId;
		this.driverName = driverName;
		this.driverLicenceNo = driverLicenceNo;
	}


	public int getDriverId() {
		return driverId;
	}


	public void setDriverId(int driverId) {
		this.driverId = driverId;
	}


	public String getDriverName() {
		return driverName;
	}


	public void setDriverName(String driverName) {
		this.driverName = driverName;
	}


	public int getDriverLicenceNo() {
		return driverLicenceNo;
	}


	public void setDriverLicenceNo(int driverLicenceNo) {
		this.driverLicenceNo = driverLicenceNo;
	}


	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return driverId+" "+driverName+" "+driverLicenceNo;
	}


	public Driver() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
	   
	   
	   
	   
	  
	
	
	

}
