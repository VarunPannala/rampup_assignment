package com.model;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id; 
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity 
@Table(name="User1")
public class User {
	
	   @Id
	   @GeneratedValue(strategy=GenerationType.AUTO)
	   private int userId;
	   private String userName;
	   private int userPancard;
	 
	  
    
	@ManyToMany
    @JoinTable(name="User_Vehicle",joinColumns= {@JoinColumn(name="userId")},inverseJoinColumns= {@JoinColumn(name="vehicleId")}) 
	private List<Vehicle> Vehicles;



	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return userId+" "+userName+" "+userPancard ;
	}



	public User() {
		super();
		// TODO Auto-generated constructor stub
	}



	public User(int userId, String userName, int userPancard, List<Vehicle> vehicles) {
		super();
		this.userId = userId;
		this.userName = userName;
		this.userPancard = userPancard;
		Vehicles = vehicles;
	}



	public int getUserId() {
		return userId;
	}



	public void setUserId(int userId) {
		this.userId = userId;
	}



	public String getUserName() {
		return userName;
	}



	public void setUserName(String userName) {
		this.userName = userName;
	}



	public int getUserPancard() {
		return userPancard;
	}



	public void setUserPancard(int userPancard) {
		this.userPancard = userPancard;
	}



	public List<Vehicle> getVehicles() {
		return Vehicles;
	}



	public void setVehicles(List<Vehicle> vehicles) {
		Vehicles = vehicles;
	}
	  
}