package com.service;

import java.util.List;

import com.model.Vehicle;

public interface VehicleServiceInterface1 {

	public Vehicle saveVehicle(Vehicle vehicle);
    public Vehicle updateVehicle(Vehicle vehicle);
    public Vehicle getvehicle(int vehicle_id);
    public void deleteVehicle(int user_id);
    public List<Vehicle> getAllVehicle();


	
}
