package com.service;

import java.util.List;

import javax.persistence.Query;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import com.model.*;
import com.utility.Utility;
public class VehicleServiceInterfaceImpl implements VehicleServiceInterface1 {

	 

	static Session session = Utility.getSession();
	Transaction transaction;

	 
	public Vehicle saveVehicle(Vehicle vehicle) {
	transaction = session.beginTransaction();
	session.save(vehicle);
	transaction.commit();
	return vehicle;
	}

	 
	


	public Vehicle updateVehicle(Vehicle vehicle) {
	session.saveOrUpdate(vehicle);
	transaction.commit();
	return vehicle;
	}

	 
    public Vehicle getvehicle(int vehicle_id) {
    Vehicle vehicle = (Vehicle) session.get(Vehicle.class, vehicle_id);
	return vehicle;
	}

	 
	public void deleteVehicle(int user_id) {
	transaction = session.beginTransaction();
	Query q=session.createQuery("delete from Vehicle where userId=:Id1");
	q.setParameter("Id1",user_id );	
	q.executeUpdate();
	transaction.commit();
	}

	 
	public List<Vehicle> getAllVehicle() {
	List<Vehicle> vehicle=session.createQuery("from Vehicle",Vehicle.class).list();
	return vehicle;
	
	 }





	





	





	



	





} 