package com.service;

import java.util.List;

import com.model.User;

public interface UserServiceInterface {
	public User saveUser(User user);
    public User updateUser(User user);
    public User getUser(int user_id);
    public void deleteUser(int user_id);
    public List<User> getAllUsers();

}
