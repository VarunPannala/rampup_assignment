package com.service;
import java.util.List;

import javax.persistence.Query;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import com.model.User;
import com.utility.Utility;
public class UserServiceInterfaceImpl implements UserServiceInterface {

	 

	static Session session = Utility.getSession();
	Transaction transaction;

	 
	public User saveUser(User user) {
	transaction = session.beginTransaction();
	session.save(user);
	transaction.commit();
	return user;
	}

	 
	


	public User updateUser(User user) {
	session.saveOrUpdate(user);
	transaction.commit();
	return user;
	}

	 
	public User getUser(int user_id) {
	User user = (User) session.get(User.class, user_id);
	return user;
	}

	 
	public void deleteUser(int user_id) {
	transaction = session.beginTransaction();
	Query q=session.createQuery("delete from User where userId=:Id1");
	q.setParameter("Id1",user_id );	
	q.executeUpdate();
	transaction.commit();
	}

	 
	public List<User> getAllUsers() {
	List<User> users=session.createQuery("from User",User.class).list();
	return users;
	
	 }
} 