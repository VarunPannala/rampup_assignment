package com.service;

import java.util.ArrayList;
import java.util.List;


import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import com.model.Driver;
import com.model.User;
import com.model.UserBooking;
import com.model.Vehicle;

public class App1 {
	
public static void main(String[] args) {
		
		System.out.println("Hellloooo8");
		
		 
		Configuration cfg=new Configuration();
		cfg.configure("hibernate.cfg.xml");
		SessionFactory factory = cfg.buildSessionFactory();
		User user1=new User();
        user1.setUserId(1);
        user1.setUserName("Ashok");
        user1.setUserPancard(1111);
        
        
        Vehicle veh1=new Vehicle();
        veh1.setVehicleId(1);
        veh1.setVehicleName("Audi");
        veh1.setVehicleRegNo(1100);
        veh1.setVehicleType("Premium");
        
        
        List<User>list1=new ArrayList<User>();
        List<Vehicle>list2=new ArrayList<Vehicle>();
        
        list1.add(user1);
        
        list2.add(veh1);
        
        user1.setVehicles(list2);
		
       
		
		Driver d=new Driver();
		d.setDriverId(1);
		d.setDriverName("Shakti");
		d.setDriverLicenceNo(1233111);
		
		UserBooking u=new UserBooking();
		u.setBooking_id(1);
		u.setDriver(d);
		u.setDestination("Aaraku");
		u.setSource("Banglore");
		u.setUser(user1);
		u.setVehicle(veh1);
		Session session= factory.openSession();
	     Transaction tx=session.beginTransaction();
	     session.save(user1);
	     session.save(veh1);
	     session.save(d);
	     
	     session.save(u);
	        tx.commit();
	        session.close();			

        

		
}

}
